﻿

using RPG_test.Classes.Character;
using RPG_test.Classes.Enums;
using RPG_test.Classes.Exceptions;
using RPG_test.Classes.Items;

class Program
{
    static void Main(string[] args)
    {
        Warrior warrior = new Warrior();
        Console.WriteLine(warrior.characterInfo());
        Mage mage = new Mage();
        Console.WriteLine(mage.characterInfo());
        Ranger ranger = new Ranger();
        Console.WriteLine(ranger.characterInfo());
        Rouge rouge = new Rouge();
        Console.WriteLine(rouge.characterInfo());

    }
    
}

