﻿using RPG_test.Classes.Enums;
using RPG_test.Classes.Exceptions;
using RPG_test.Classes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Character
{
    public abstract class Character
    {
        public Character()
        {
            PreInitialization();
            name = "";
            level = 1;
            //weapon damage is set to 1 when a character has no weapon
            weaponDamage = 1;
            calcDamage();
            inventory = new Dictionary<Slots, object>();
        }
        private protected string name;
        private protected Dictionary<Slots, object> inventory;
        private protected Slots[] avaliableSlots = { Slots.Head, Slots.Body, Slots.Legs, Slots.Arms };
        private protected ArmorType[] equipableArmor;
        private protected WeaponTypes[] equipableWeapons;
        private protected string charClass;
        private protected Stats totalStats;
        private protected double weaponDamage;
        private protected double totalDamage;
        private protected int level;
        private protected Stats levelGain;

        public string Name { get => name; }
        public Dictionary<Slots, object> Inventory { get => inventory; }
        public string CharClass { get => charClass;}
        public Stats TotalStats { get => totalStats;}
        public double WeaponDamage { get => weaponDamage;}
        public double TotalDamage { get => totalDamage; }
        public int Level { get => level;}
        public WeaponTypes[] EquipableWeapons { get => equipableWeapons;}
        public ArmorType[] EquipableArmour { get => equipableArmor; }
        /// <summary>
        /// Method to equipe any weapon who inherits from the 
        /// IItem-interface(in this case "weapon" and "armour".
        /// </summary>
        /// <param name="toEquip"></param>
        /// <returns></returns>
        /// <exception cref="WrongTypeException"></exception>
        /// <exception cref="NotAnItemExecption"></exception>
        /// <exception cref="TooHighLevelException"></exception>
        public string equipe(IItem toEquip)
        {
            if (toEquip.ReqLevel <= level)
            {
                if(toEquip.GetType().Equals(new Weapons().GetType()))
                {
                    if (equipableWeapons.Contains((WeaponTypes)toEquip.Type))
                    {
                        //equipe to "inventory"
                        //removes the stats of the previous item if there is an existing which gets unequiped
                        if (inventory.ContainsKey(toEquip.Slot))
                        {
                            Weapons temp = (Weapons)inventory[toEquip.Slot];
                            this.inventory[toEquip.Slot] = toEquip;
                            WeaponStats toEquiWeaponStats = (WeaponStats)toEquip.Stats;
                            weaponDamage = toEquiWeaponStats.DamagePerSecond;
                        }
                        else
                        {
                            this.inventory.Add(toEquip.Slot, toEquip);
                            WeaponStats toEquiWeaponStats = (WeaponStats)toEquip.Stats;
                            weaponDamage = toEquiWeaponStats.DamagePerSecond;
                        }
                        calcDamage();
                        return "New weapon equipped!";
                    }
                    else
                    {
                        //throw error for wrong armour
                        throw new WrongTypeException();
                    }
                }
                else if (toEquip.GetType().Equals(new Armor().GetType()))
                {
                    if (equipableArmor.Contains((ArmorType)toEquip.Type))
                    {
                        //equipe to "inventory"
                        //removes the stats of the previous item if there is an existing which gets unequiped
                        if (inventory.ContainsKey(toEquip.Slot))
                        {
                            Armor temp = (Armor)inventory[toEquip.Slot];
                            totalStats -= (Stats)temp.Stats;
                            this.inventory[toEquip.Slot] = toEquip;
                            totalStats += (Stats)toEquip.Stats;
                        }
                        else
                        {
                            this.inventory.Add(toEquip.Slot, toEquip);
                            totalStats += (Stats)toEquip.Stats;
                        }
                        calcDamage();
                        return "New armor equipped!";
                    }
                    else
                    {
                        //throw error for wrong armour
                        throw new WrongTypeException();
                    }
                }
                else
                {
                    throw new NotAnItemExecption();
                }
            }
            else
            {
                //throw error for too low level
                throw new TooHighLevelException();
            }
        }
        /// <summary>
        /// Leveling up the character by incrementing the level-value
        /// adding the levelGain which is specified in the classes of 
        /// the specific hero/character(mage, rouge... etc.). 
        /// </summary>
        public void levelUp()
        {
            level++;
            totalStats += levelGain;
            calcDamage();
        }
        /// <summary>
        /// Calculating the damage of the character based on 
        /// their own specific multipliers(which skill increases 
        /// their damage) and the items that might be equipped.
        /// </summary>
        public abstract void calcDamage();
        /// <summary>
        /// Returns the info of the character as a string to be 
        /// used in the console.
        /// </summary>
        /// <returns></returns>
        public string characterInfo()
        {
            string output = $"\n\n\nCharacter Class: {charClass}\nCharacter level: {level}\nStrength: {totalStats.strength}\nDexterity: {totalStats.dexterity}\nIntelligence: {totalStats.intelligence}\nDamage: {TotalDamage}\n ";
            return output;
        }
        /// <summary>
        /// Abstract function that sets the basePrimaryAttributes
        /// and totalPrimaryAttribute in the sub-classes to be initilized 
        /// in the constructor before the values are used in 
        /// "calcDamage"-function.
        /// </summary>
        protected abstract void PreInitialization();
    }
}
