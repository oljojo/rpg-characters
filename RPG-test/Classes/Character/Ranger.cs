﻿using RPG_test.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Character
{
    public class Ranger: Character
    {
        public Ranger()
        {
            name = "defautRanger";
            charClass = "Ranger";
            equipableWeapons = new WeaponTypes[] {WeaponTypes.Bow};
            equipableArmor = new ArmorType[] { ArmorType.Leather, ArmorType.Mail};
            levelGain = new Stats(1, 5, 1);
        }
        private Stats LevelGain { get => levelGain; }
        /// <summary>
        /// Calculates the damage of the character based on weapon and dexterity
        /// </summary>
        public override void calcDamage()
        {
            totalDamage = weaponDamage * (1 + (0.01 * totalStats .dexterity));
        }
        /// <summary>
        /// Sets the initial stats of "Ranger"
        /// </summary
        protected override void PreInitialization()
        {
            totalStats = new Stats(1, 7, 1);
        }
    }
}
