﻿using RPG_test.Classes.Enums;
using RPG_test.Classes.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Character
{
    public class Mage : Character
    {
        public Mage()
        {
            name = "defaultMage";
            charClass = "Mage";
            equipableWeapons = new WeaponTypes[] { WeaponTypes.Wands, WeaponTypes.Staffs};
            equipableArmor = new ArmorType[] { ArmorType.Cloth };
            levelGain = new Stats(1, 1, 5);

        }
        public Stats LevelGain { get => levelGain; }
        /// <summary>
        /// Calculates the damage based on weapon-stats and the intelligence of the character stats
        /// </summary>
        public override void calcDamage()
        {
            totalDamage = weaponDamage * (1 + (0.01 * totalStats.intelligence));
        }
        /// <summary>
        /// Sets the initial stats of "Mage"
        /// </summary>
        protected override void PreInitialization()
        {
            totalStats = new Stats(1, 1, 8);
        }
    }
}
