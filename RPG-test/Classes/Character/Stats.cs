﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Character
{
    public class Stats
    {
        public Stats()
        {
            strength = 0;
            dexterity = 0;
            intelligence = 0;
        }
        public Stats(int strth, int dex, int intelli)
        {
            strength = strth;
            dexterity = dex;
            intelligence = intelli;
        }

        public int strength { get; }
        public int dexterity { get; }
        public int intelligence { get; }

        public override bool Equals(object? obj)
        {
            return obj is Stats stats &&
                   strength == stats.strength &&
                   dexterity == stats.dexterity &&
                   intelligence == stats.intelligence;
        }
        /// <summary>
        /// Makes it possible to add two Stats-object-values together
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Stats operator +(Stats lhs, Stats rhs)
        {
            return new Stats(lhs.strength + rhs.strength, lhs.dexterity + rhs.dexterity, lhs.intelligence + rhs.intelligence);
        }
        /// <summary>
        /// Makes it possible to subtract one Stats-object-values from another
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Stats operator -(Stats lhs, Stats rhs)
        {
            return new Stats(lhs.strength - rhs.strength, lhs.dexterity - rhs.dexterity, lhs.intelligence - rhs.intelligence);
        }
        /// <summary>
        /// Makes it possible to multipy the values of two Stats-objects
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Stats operator *(Stats lhs, Stats rhs)
        {
            return new Stats(lhs.strength * rhs.strength, lhs.dexterity * rhs.dexterity, lhs.intelligence * rhs.intelligence);
        }
        /// <summary>
        /// Multiplies all the values of the Stats object with a number(int)
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns></returns>
        public static Stats operator *(Stats lhs, int rhs)
        {
            return new Stats(lhs.strength * rhs, lhs.dexterity * rhs, lhs.intelligence * rhs);
        }


    }


}
