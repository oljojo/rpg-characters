﻿using RPG_test.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Character
{
    public class Warrior: Character
    {
        public Warrior()
        {
            name = "defaultWarrior";
            charClass = "Warrior";
            this.equipableWeapons = new WeaponTypes[] { WeaponTypes.Axe, WeaponTypes.Hammers, WeaponTypes.Swords };
            this.equipableArmor = new ArmorType[] { ArmorType.Plate, ArmorType.Mail};
            this.levelGain = new Stats(3, 2, 1);
        }

        
        
        public Stats LevelGain { get => levelGain; }
        /// <summary>
        /// Calculates the damage of the character based on weapon and strength
        /// </summary>
        public override void calcDamage()
        {
            totalDamage = weaponDamage * (1 + (0.01 * (double)totalStats.strength));
        }
        /// <summary>
        /// Sets the initial stats for "Warrior
        /// </summary>
        protected override void PreInitialization()
        {
            this.totalStats = new Stats(5, 2, 1);
        }
    }
}

