﻿using RPG_test.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Character
{
    public class Rouge: Character
    {
        public Rouge()
        {
            name = "defaultRouge";
            charClass = "Rouge";
            this.equipableWeapons = new WeaponTypes[] { WeaponTypes.Daggers, WeaponTypes.Swords};
            this.equipableArmor = new ArmorType[] { ArmorType.Leather, ArmorType.Mail };
            levelGain = new Stats(1, 4, 1);

        }
        private Stats LevelGain { get => levelGain; }
        /// <summary>
        /// Calculates the damage of the character based on weapon and dexterity
        /// </summary>
        public override void calcDamage()
        {
            totalDamage = weaponDamage * (1 + (0.01 * totalStats.dexterity));
        }
        /// <summary>
        /// Sets the initial stats of "Rouge"
        /// </summary>
        protected override void PreInitialization()
        {
            this.totalStats = new Stats(2, 6, 1);
        }
    }
}
