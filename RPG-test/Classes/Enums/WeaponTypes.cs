﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Enums
{
    /// <summary>
    /// Enum for all the avaliable Weapon-Types
    /// </summary>
    public enum WeaponTypes
    {
        Axe,
        Bow,
        Daggers,
        Hammers,
        Staffs, 
        Swords, 
        Wands
    }
}
