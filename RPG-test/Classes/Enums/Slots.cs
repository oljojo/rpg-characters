﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Enums
{
    /// <summary>
    /// Enum for all the avaliable Slots on the character
    /// </summary>
    public enum Slots
    {
        Head,
        Body, 
        Legs,
        Arms,
        Weapon

    }
}
