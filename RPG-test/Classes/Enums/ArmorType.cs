﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Enums
{
    /// <summary>
    /// Enum for all the avaliable Armor-Types
    /// </summary>
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
