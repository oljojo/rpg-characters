﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Items
{
    public class WeaponStats
    {


        private double damage;
        private double attackSpeed;

        public double Damage { get => damage; set => damage = value; }
        public double AttackSpeed { get => attackSpeed; set => attackSpeed = value; }
        public double DamagePerSecond { get => attackSpeed * damage; }

        public override bool Equals(object? obj)
        {
            return obj is WeaponStats stats &&
                   damage == stats.damage &&
                   attackSpeed == stats.attackSpeed &&
                   Damage == stats.Damage &&
                   AttackSpeed == stats.AttackSpeed &&
                   DamagePerSecond == stats.DamagePerSecond;
        }
    }
}
