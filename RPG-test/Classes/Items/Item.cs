﻿using RPG_test.Classes.Character;
using RPG_test.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Items
{
    public interface IItem
    {

        public string Name { get; set; }
        public int ReqLevel { get; set; }
        public Slots Slot { get; set; }
        public object Stats { get; set; }
        public object Type { get; set; }

    }
}
