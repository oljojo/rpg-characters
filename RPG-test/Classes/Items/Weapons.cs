﻿using RPG_test.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Items
{
    public class Weapons: IItem
    {

        private WeaponTypes type;
        private WeaponStats stats;

        public double Damage { get => stats.Damage; }
        public double AttackSpeed { get => stats.AttackSpeed; }
        public double DamagePerSecond { get => stats.DamagePerSecond; }
        public string Name { get; set; }
        public int ReqLevel { get; set; }
        public Slots Slot { get; set; }
        public object Stats { get => stats; set => stats = (WeaponStats)value; }
        public object Type { get => type; set => type = (WeaponTypes)value; }

        public override bool Equals(object? obj)
        {
            return obj is Weapons weapons &&
                   type == weapons.type &&
                   EqualityComparer<WeaponStats>.Default.Equals(stats, weapons.stats) &&
                   Damage == weapons.Damage &&
                   AttackSpeed == weapons.AttackSpeed &&
                   DamagePerSecond == weapons.DamagePerSecond &&
                   Name == weapons.Name &&
                   ReqLevel == weapons.ReqLevel &&
                   Slot == weapons.Slot &&
                   EqualityComparer<object>.Default.Equals(Stats, weapons.Stats) &&
                   EqualityComparer<object>.Default.Equals(Type, weapons.Type);
        }
    }
}
