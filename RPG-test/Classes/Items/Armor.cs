﻿using RPG_test.Classes.Character;
using RPG_test.Classes.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Items
{
    public class Armor : IItem
    {
        private ArmorType type;
        private Stats stats;
        public string Name { get; set; }
        public int ReqLevel { get; set; }
        public Slots Slot { get; set; }
        public object Stats { get => stats; set => stats = (Stats)value; }
        public object Type { get => type; set => type = (ArmorType)value; }

        public override bool Equals(object? obj)
        {
            return obj is Armor armor &&
                   type == armor.type &&
                   EqualityComparer<Stats>.Default.Equals(stats, armor.stats) &&
                   Name == armor.Name &&
                   ReqLevel == armor.ReqLevel &&
                   Slot == armor.Slot &&
                   EqualityComparer<object>.Default.Equals(Stats, armor.Stats) &&
                   EqualityComparer<object>.Default.Equals(Type, armor.Type);
        }
    }
}
