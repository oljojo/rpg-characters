﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Exceptions
{
    /// <summary>
    /// Exception that is thrown if an object that is not an item.
    /// </summary>
    public class NotAnItemExecption : Exception
    {
        public override string Message => "This is not an item and cannot be equipped!";
    }
}

