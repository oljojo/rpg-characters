﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Exceptions
{
    /// <summary>
    /// Exception that is thrown if the required level of the item is higher than the character
    /// </summary>
    public class TooHighLevelException: Exception
    {
        public override string Message => "Item has too high required level, it doesn't fit your low-level!";
    }
}
