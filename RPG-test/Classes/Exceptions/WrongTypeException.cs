﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_test.Classes.Exceptions
{
    /// <summary>
    /// Exception that is thrown if the item is of wrong type to the character.
    /// </summary>
    public class WrongTypeException: Exception
    {
        public override string Message => "Item is of wrong type, it doesn't fit your class!";
    }
}
