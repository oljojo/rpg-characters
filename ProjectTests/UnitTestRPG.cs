


using RPG_test.Classes.Character;
using RPG_test.Classes.Enums;
using RPG_test.Classes.Exceptions;
using RPG_test.Classes.Items;

namespace ProjectTests
{
    public class UnitTestRPG
    {
        /// <summary>
        /// Character tests without items
        /// </summary>
        [Fact]
        public static void Constructor_InitiatingAMage_InitalLevelToBe1()
        {
            //Arrange
                //expected
            int expectedLevel = 1;
            //Act
                //actual
            Mage testRanger = new Mage();
            int actualLevel = testRanger.Level;
            //Assert
                //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void Constructor_InitiatingARanger_InitalLevelToBe1()
        {
            //Arrange
            //expected
            int expectedLevel = 1;
            //Act
            //actual
            Ranger testRanger = new Ranger();
            int actualLevel = testRanger.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void Constructor_InitiatingARouge_InitalLevelToBe1()
        {
            //Arrange
            //expected
            int expectedLevel = 1;
            //Act
            //actual
            Rouge testRouge = new Rouge();
            int actualLevel = testRouge.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void Constructor_InitiatingAWarrior_InitalLevelToBe1()
        {
            //Arrange
            //expected
            int expectedLevel = 1;
            //Act
            //actual
            Warrior testWarrior = new Warrior();
            int actualLevel = testWarrior.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void LevelUp_LevelingUpMageOnce_LevelToBe2()
        {
            //Arrange
            //expected
            int expectedLevel = 2;
            //Act
            //actual
            Mage mageLevel2 = new Mage();
            mageLevel2.levelUp();
            int actualLevel = mageLevel2.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void LevelUp_LevelingUpRangerOnce_LevelToBe2()
        {
            //Arrange
            //expected
            int expectedLevel = 2;
            //Act
            //actual
            Ranger rangerLevel2 = new Ranger();
            rangerLevel2.levelUp();
            int actualLevel = rangerLevel2.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void LevelUp_LevelingUpRougeOnce_LevelToBe2()
        {
            //Arrange
            //expected
            int expectedLevel = 2;
            //Act
            //actual
            Rouge rougeLevel2 = new Rouge();
            rougeLevel2.levelUp();
            int actualLevel = rougeLevel2.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void LevelUp_LevelingUpWarriorOnce_LevelToBe2()
        {
            //Arrange
            //expected
            int expectedLevel = 2;
            //Act
            //actual
            Warrior warriorLevel2 = new Warrior();
            warriorLevel2.levelUp();
            int actualLevel = warriorLevel2.Level;
            //Assert
            //equal
            Assert.Equal(expectedLevel, actualLevel);
        }
        [Fact]
        public static void StatsMage_InitialStatsOfMage_ToBe1_1_8()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(1, 1, 8);
            //Act
            //actual
            Mage initMage = new Mage();
            Stats actualStats = initMage.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        [Fact]
        public static void StatsRanger_InitialStatsOfRange_ToBe1_7_1()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(1, 7, 1);
            //Act
            //actual
            Ranger initRanger = new Ranger();
            Stats actualStats = initRanger.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        [Fact]
        public static void StatsWarrior_InitialStatsOfWarrior_ToBe5_2_1()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(5, 2, 1);
            //Act
            //actual
            Warrior initWarrior = new Warrior();
            Stats actualStats = initWarrior.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        [Fact]
        public static void StatsRouge_InitialStatsOfRouge_ToBe2_6_1()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(2, 6, 1);
            //Act
            //actual
            Rouge initRouge = new Rouge();
            Stats actualStats = initRouge.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }

        [Fact]
        public static void StatsMage_Level2StatsOfMage_ToBe2_2_13()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(2, 2, 13);
            //Act
            //actual
            Mage level2Mage = new Mage();
            level2Mage.levelUp();
            Stats actualStats = level2Mage.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        [Fact]
        public static void StatsRanger_Level2StatsOfRange_ToBe2_12_2()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(2, 12, 2);
            //Act
            //actual
            Ranger level2Ranger = new Ranger();
            level2Ranger.levelUp();
            Stats actualStats = level2Ranger.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        [Fact]
        public static void StatsWarrior_Level2StatsOfWarrior_ToBe8_4_2()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(8, 4, 2);
            //Act
            //actual
            Warrior level2Warrior = new Warrior();
            level2Warrior.levelUp();
            Stats actualStats = level2Warrior.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        [Fact]
        public static void StatsRouge_Level2StatsOfRouge_ToBe3_10_2()
        {
            //Arrange
            //expected
            Stats expectedStats = new Stats(3, 10, 2);
            //Act
            //actual
            Rouge level2Rouge = new Rouge();
            level2Rouge.levelUp();
            Stats actualStats = level2Rouge.TotalStats;
            //Assert
            //equal
            Assert.Equal(expectedStats, actualStats);
        }
        /// <summary>
        /// Item test without characters
        /// </summary>
        [Fact]
        public static void Weapon_CreateWeapon_ToBeCreated()
        {
            //Arrange
            //expected
            Weapons expectedWeapon = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };

            //Act
            //actual
            Weapons testAxe = new Weapons()
            {   
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1}
            };
            //Assert
            //equal
            Assert.Equal(expectedWeapon, testAxe);
        }
        /// <summary>
        /// Item tests with characters
        /// </summary>
        [Fact]
        public static void EquipWeapon_EquipeWeaponToWarrior_ToBeEquipedToWarrior()
        {
            //Arrange
            //expected
            Weapons expectedWeapon = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };

            //Act
            //actual
            Warrior testWarrior = new Warrior();
            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };
            testWarrior.equipe(testAxe);
            Weapons equipedWeapon = (Weapons)testWarrior.Inventory[Slots.Weapon];
            //Assert
            //equal
            Assert.Equal(expectedWeapon, equipedWeapon);
        }
        [Fact]
        public static void EquipWeapon_EquipeWeaponToWarrior_ToReturnEquipedMessage()
        {
            //Arrange
            //expected
            string expectedEquipMessage = "New weapon equipped!";


            //Act
            //actual
            Warrior testWarrior = new Warrior();
            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };
            string equipMessage = testWarrior.equipe(testAxe);
            //Assert
            //equal
            Assert.Equal(expectedEquipMessage, equipMessage);
        }
        [Fact]
        public static void EquipArmor_EquipeArmorToWarrior_ToReturnEquipedMessage()
        {
            //Arrange
            //expected
            string expectedEquipMessage = "New armor equipped!";

            //Act
            //actual
            Warrior testWarrior = new Warrior();
            Armor testPlateArmor = new Armor()
            {
                Name = "Common plate body armor",
                ReqLevel = 1,
                Slot = Slots.Body,
                Type = ArmorType.Plate,
                Stats = new Stats(1, 0, 0)
            };
            string equipMessage = testWarrior.equipe(testPlateArmor);
            //Assert
            //equal
            Assert.Equal(expectedEquipMessage, equipMessage);
        }
        [Fact]
        public static void EquipArmor_EquipeArmorToWarrior_ToThrowItemLevelError()
        {
            //Arrange
            //expected

            //Act
            //actual
            Warrior testWarrior = new Warrior();
            Armor testPlateArmor = new Armor()
            {
                Name = "Common plate body armor",
                ReqLevel = 2,
                Slot = Slots.Body,
                Type = ArmorType.Plate,
                Stats = new Stats(1, 0, 0)
            };
            //Assert
            //equal
            Assert.Throws<TooHighLevelException>(() => testWarrior.equipe(testPlateArmor));
        }
        [Fact]
        public static void EquipWeapon_EquipeWeaponToWarrior_ToThrowItemClassError()
        {
            //Arrange
            //expected
            TooHighLevelException expectedError = new TooHighLevelException();

            //Act
            //actual
            Warrior testWarrior = new Warrior();
            Weapons testBow = new Weapons()
            {
                Name = "Common bow",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Bow,
                Stats = new WeaponStats() { Damage = 12, AttackSpeed = 0.8 }
            };

            //Assert
            //equal
            Assert.Throws<WrongTypeException>(() => testWarrior.equipe(testBow));
            //Assert.Equal(expectedError, equipedWeapon);
        }
        /// <summary>
        /// Character tests with (equipped) items
        /// </summary>
        [Fact]
        public static void CharacterDamage_DisplayDamageOfLevel1Warrior_ToBeTheExpectedDamage()
        {
            //Arrange
            //expected
            double expectedDamage = 1.0 * (1.0 + (5.0 / 100.0));


            //Act
            //actual
            Warrior testWarrior = new Warrior();
            double warriorDamage = testWarrior.TotalDamage;

            //Assert
            //equal
            Assert.Equal(expectedDamage, warriorDamage);
            //Assert.Equal(expectedError, equipedWeapon);
        }
        [Fact]
        public static void CharacterDamage_DisplayDamageOfWarriorWithAxe_ToBeTheExpectedDamage()
        {
            //Arrange
            //expected
            double expectedDamage = (7.0 * 1.1) * (1.0 + (5.0 / 100.0));


            //Act
            //actual
            Warrior testWarrior = new Warrior();
            
            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };
            testWarrior.equipe(testAxe);
            double warriorDamage = testWarrior.TotalDamage;
            //Assert
            //equal
            Assert.Equal(expectedDamage, warriorDamage);
            //Assert.Equal(expectedError, equipedWeapon);
        }
        [Fact]
        public static void CharacterDamage_DisplayDamageOfWarriorWithAxeAndArmor_ToBeTheExpectedDamage()
        {
            //Arrange
            //expected
            double expectedDamage = ((7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0)));


            //Act
            //actual
            Warrior testWarrior = new Warrior();

            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateArmor = new Armor()
            {
                Name = "Common plate body armor",
                ReqLevel = 1,
                Slot = Slots.Body,
                Type = ArmorType.Plate,
                Stats = new Stats(1, 0, 0)
            };
            testWarrior.equipe(testAxe);
            testWarrior.equipe(testPlateArmor);
            double warriorDamage = testWarrior.TotalDamage;
            //Assert
            //equal
            Assert.Equal(expectedDamage, warriorDamage);
        }
        [Fact]
        public static void CharacterDamage_DisplayDamageOfWarriorWithAxeAndArmorEquipeUnEquipe_ToBeTheExpectedDamage()
        {
            //Arrange
            //expected
            double expectedDamage = ((10.0 * 1.5) * (1.0 + ((5.0 + 3.0) / 100.0)));


            //Act
            //actual
            Warrior testWarrior = new Warrior();

            Weapons testAxe = new Weapons()
            {
                Name = "Common axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor testPlateArmor = new Armor()
            {
                Name = "Common plate body armor",
                ReqLevel = 1,
                Slot = Slots.Body,
                Type = ArmorType.Plate,
                Stats = new Stats(1, 0, 0)
            };
            Weapons testAxe2 = new Weapons()
            {
                Name = "UnCommon axe",
                ReqLevel = 1,
                Slot = Slots.Weapon,
                Type = WeaponTypes.Axe,
                Stats = new WeaponStats() { Damage = 10, AttackSpeed = 1.5 }
            };
            Armor testPlateArmor2 = new Armor()
            {
                Name = "UnCommon plate body armor",
                ReqLevel = 1,
                Slot = Slots.Body,
                Type = ArmorType.Plate,
                Stats = new Stats(3, 0, 0)
            };
            //equipe the first axe and armor
            testWarrior.equipe(testAxe);
            testWarrior.equipe(testPlateArmor);

            //equipe new axe and armor to replace the old ones and removing their stats
            testWarrior.equipe(testAxe2);
            testWarrior.equipe(testPlateArmor2);

            double warriorDamage = testWarrior.TotalDamage;
            //Assert
            //equal
            Assert.Equal(expectedDamage, warriorDamage);
        }
    }
}