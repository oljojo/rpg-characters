# RPG Characters

A .NET Core console application.

Has the basic functionality for four character/hero classes similar to a common RPG-game.

## Usage

* Clone repository to a local directory
* Open the .sln file in Visual Studio
* Run tests to verify functionality.

The app will display the character info for a 1-level character of each class when run. These may be altered by level-up or equipping an item.

## Prerequisites
Visual Studio  
.Net 6.0

## Contributing
[Olof Johnsson](@oljojo)


